<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = "register_officers";
    protected $fillable = ['ro_code', 'name', 'nik', 'email', 'password', 'birthdate', 'phone', 'image'];
    protected $hidden = [
        'password'
    ];
}
