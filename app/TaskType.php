<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskType extends Model
{
    //
    protected $table = 'task_types';
    protected $fillable = ['title', 'slug', 'is_deleted'];
}
